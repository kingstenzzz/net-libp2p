VERSION=v2.2.0
NET_VERSION=v1.1.0
gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION)
	go get chainmaker.org/chainmaker/protocol/v2@$(VERSION)
	go get chainmaker.org/chainmaker/libp2p-pubsub@v1.1.1_qc
	go get chainmaker.org/chainmaker/net-common@$(NET_VERSION)
	go get chainmaker.org/chainmaker/logger/v2@$(VERSION)
	go mod tidy
