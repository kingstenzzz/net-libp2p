module chainmaker.org/chainmaker/net-libp2p

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.2.0
	chainmaker.org/chainmaker/libp2p-pubsub v1.1.1
	chainmaker.org/chainmaker/logger/v2 v2.2.0
	chainmaker.org/chainmaker/net-common v1.1.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.0
	github.com/libp2p/go-libp2p v0.11.0
	github.com/libp2p/go-libp2p-circuit v0.3.1
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/libp2p/go-libp2p-kad-dht v0.10.0
	github.com/multiformats/go-multiaddr v0.3.2
	github.com/stretchr/testify v1.7.0
	github.com/tjfoc/gmsm v1.4.1
)

replace github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v1.0.0
